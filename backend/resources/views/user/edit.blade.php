@extends('layouts.app')

@section('content')

    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">プロフィール変更</h1>
    </div>

    <div class="row justify-content-center">
        {{-- Area Chart --}}
        <div class="col-xl-8 col-lg-12">

            <div class="card shadow mb-4">
                <form method="POST" action="{{ route('user.update') }}" class="user">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    {{-- Card Header --}}
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    </div>


                    {{-- Card Body --}}
                    <div class="card-body">
                        <div class="container">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="form-group">
                                <label class="mt-2" for="name">ユーザー名</label>
                                <input type="text"
                                    class="mb-2 form-control form-control-user @error('name') is-invalid @enderror"
                                    id="name" name="name" value="{{ old('name', $user->name) }}">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="mt-2" for="email">メールアドレス</label>
                                <input type="email"
                                    class="mb-2 form-control form-control-user @error('email') is-invalid @enderror"
                                    id="email" name="email" value="{{ old('email', $user->email) }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <hr>
                            <button type="submit" class="mt-4 btn btn-primary btn-user btn-block">
                                {{ __('更新する') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
