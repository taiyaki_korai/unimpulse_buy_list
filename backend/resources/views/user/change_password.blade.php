@extends('layouts.app')

@section('content')

    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">パスワード変更</h1>
    </div>

    <div class="row justify-content-center">
        {{-- Area Chart --}}
        <div class="col-xl-8 col-lg-12">

            <div class="card shadow mb-4">
                <form method="POST" action="{{ route('user.update') }}" class="user">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    {{-- Card Header --}}
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    </div>


                    {{-- Card Body --}}
                    <div class="card-body">
                        <div class="container">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="form-group">
                                <label class="mt-2" for="old-password">現在のパスワード</label>
                                <input type="password"
                                    class="mb-2 form-control form-control-user @error('old_password') is-invalid @enderror"
                                    id="old-password" name="old_password" value="{{ old('old_password') }}">
                                @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="mt-2" for="password">新しいパスワード</label>
                                <input type="password"
                                    class="mb-2 form-control form-control-user @error('password') is-invalid @enderror"
                                    id="password" name="password" value="{{ old('password') }}">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="mt-2" for="password-confirmation">新しいパスワード（確認）</label>
                                <input type="password"
                                    class="mb-2 form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                                    id="password-confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <hr>
                            <button type="submit" class="mt-4 btn btn-primary btn-user btn-block">
                                {{ __('パスワードを変更する') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
