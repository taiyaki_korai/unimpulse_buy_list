@extends('layouts.app')

@section('content')

    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">欲しいもの編集</h1>
    </div>

    <div class="row justify-content-center">
        {{-- Area Chart --}}
        <div class="col-xl-8 col-lg-12">

            <div class="card shadow mb-4">
                <form method="POST" action="{{ route('wish_thing.update', $wishThing->id) }}" class="user">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    {{-- Card Header --}}
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    </div>


                    {{-- Card Body --}}
                    <div class="card-body">
                        <div class="container">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="name">
                                    <h3>欲しいもの</h3>
                                </label>
                                <input type="text"
                                    class="mb-4 form-control form-control-user @error('name') is-invalid @enderror"
                                    id="name" name="name" value="{{ old('name', $wishThing->name) }}">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="mt-2" for="price">値段</label>
                                <input type="number"
                                    class="mb-2 form-control form-control-user @error('price') is-invalid @enderror"
                                    id="price" name="price" value="{{ old('price', $wishThing->price) }}">
                                @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="priority">欲しい度</label>
                                <input type="range"
                                    class="mb-2 form-control-range form-control-user @error('priority') is-invalid @enderror"
                                    id="priority" name="priority" value="{{ old('priority', $wishThing->priority) }}">
                                @error('priority')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="time_limit">期限</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="customRadio1">なし</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">あり</label>
                                </div>
                                {{-- TODO:期限ありの場合に期限フォームを表示 --}}
                                <input type="radio"
                                    class="mb-4 custom-control-input form-control-user @error('time_limit') is-invalid @enderror"
                                    id="time_limit" name="time_limit" value="{{ old('time_limit', $wishThing->time_limit) }}">
                                @error('time_limit')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <hr>
                            <button type="submit" class="mt-4 btn btn-primary btn-user btn-block">
                                {{ __('更新する') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
