@extends('layouts.app')

@section('content')
    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">買ったもの履歴</h1>
    </div>

    {{-- Content Row --}}
    <div class="row">
        {{-- Area Chart --}}
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                {{-- Card Header - Dropdown --}}
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">買ったもの履歴</h6>
                </div>
                {{-- Card Body --}}
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- 買ったものリスト --}}
                    <table class="table">
                        <thead>
                            <tr>
                              <th>購入日</th>
                              <th>商品名</th>
                              <th>値段</th>
                              <th></th>
                            </tr>
                          </thead>
                        @foreach ($purchased_list as $index => $wishThing)
                        
                          <tbody>
                            <tr>
                                <th scope="row">{{ $wishThing->purchase_date->format('Y/m/d') }}</th>
                                <td>{{ $wishThing->name }}</td>
                                <td>{{ $wishThing->price }}円</td>
                                <td>
                                    <a class="nav-link" href="#" data-toggle="modal" data-target="#resetPurchaseModal">
                                        <i class="fas fa-reply" data-toggle="tooltip" data-placement="top" title="欲しいものリストに戻す"></i>
                                    </a>
                                    <form id="reset-purchase-form" action="{{ route('wish_thing.reset_purchase', $wishThing->id) }}" method="POST" style="display: none;">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                          </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
