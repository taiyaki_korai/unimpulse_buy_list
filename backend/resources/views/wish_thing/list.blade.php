@extends('layouts.app')

@section('content')
    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">欲しいものリスト</h1>
    </div>

    {{-- Content Row --}}
    <div class="row">
        {{-- Area Chart --}}
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                {{-- Card Header - Dropdown --}}
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">欲しいものリスト</h6>
                </div>
                {{-- Card Body --}}
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- 欲しいものリスト --}}
                    <table class="table">
                        @foreach ($wish_list as $index => $wishThing)
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ $wishThing->name }}</td>
                            <td>{{ $wishThing->price }}円</td>
                            <td>
                                <a class="nav-link" href="{{ route('wish_thing.edit', $wishThing->id) }}">
                                    <i class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="編集する"></i>
                                </a>
                            </td>
                            <td>
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#deleteWishThingModal">
                                    <i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top" title="削除する"></i>
                                </a>
                                <form id="delete-wish-thing-form" action="{{ route('wish_thing.destroy', $wishThing->id) }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
