{{-- Sidebar --}}
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    {{-- Sidebar - Brand --}}
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fab fa-creative-commons-nc-jp"></i>
        </div>
        <div class="sidebar-brand-text mx-3 text-nowrap">衝動買わないリスト</div>
    </a>

    {{-- Divider --}}
    <hr class="sidebar-divider my-0">

    {{-- Nav Item - Dashboard --}}
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>ダッシュボード</span></a>
    </li>

    {{-- Divider --}}
    <hr class="sidebar-divider">

    {{-- Heading --}}
    <div class="sidebar-heading">
        Page Menu
    </div>

    {{-- Nav Item - Pages Collapse Menu --}}
    <li class="nav-item">
        <a class="nav-link" href="{{ route('wish_thing.create') }}">
            <i class="fas fa-plus-circle"></i>
            <span>欲しいもの追加</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('wish_thing.list') }}">
            <i class="fas fa-edit"></i>
            <span>欲しいもの編集・削除</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('budget.edit') }}">
            <i class="fas fa-calculator"></i>
            <span>買い物予算変更</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('wish_thing.history') }}">
            <i class="fas fa-history"></i>
            <span>買った物履歴</span>
        </a>
    </li>

    {{-- Nav Item - Utilities Collapse Menu --}}
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>ユーザー管理</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('user.edit') }}">プロフィール変更</a>
                <a class="collapse-item" href="{{ route('user.change_password') }}">パスワード変更</a>
                <a class="collapse-item" href="#" data-toggle="modal" data-target="#logoutModal">ログアウト</a>
            </div>
        </div>
    </li>

    {{-- Divider --}}
    <hr class="sidebar-divider">

    {{-- Sidebar Toggler (Sidebar) --}}
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
{{-- End of Sidebar --}}
