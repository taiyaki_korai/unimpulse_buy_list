{{-- Purchase Modal --}}
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    購入しますか?
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button"
                 onclick="event.preventDefault();document.getElementById('purchase-form').submit();">
                    買う
                </button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">
                    買わない
                </button>
            </div>
        </div>
    </div>
</div>
