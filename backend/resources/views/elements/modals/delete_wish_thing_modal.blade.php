{{-- delete_wish_thing_modal --}}
<div class="modal fade" id="deleteWishThingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">削除しますか?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="#" onclick="event.preventDefault();
                 document.getElementById('delete-wish-thing-form').submit();">削除する</a>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>
