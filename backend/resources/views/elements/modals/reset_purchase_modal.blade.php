{{-- reset_purchase_modal --}}
<div class="modal fade" id="resetPurchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">この商品を欲しいものリストに戻しますか?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="#" onclick="event.preventDefault();
                 document.getElementById('reset-purchase-form').submit();">欲しいものリストに戻す</a>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>
