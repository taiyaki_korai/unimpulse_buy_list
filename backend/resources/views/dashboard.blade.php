@extends('layouts.app')

@section('content')
    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <div class="justify-content-right">
            <a href="{{ route('wish_thing.create') }}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus-circle fa-sm text-white-50"></i>&nbsp;欲しいもの追加
            </a>
            <a href="{{ route('budget.edit') }}" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-calculator fa-sm text-white-50"></i>&nbsp;買い物予算変更
            </a>
        </div>
    </div>

    <div class="row">
        {{-- Card Example --}}
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                次買うべきもの</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                @if (empty($wish_list->first()))
                                    なし
                                @else
                                    {{ $wish_list->first()->name }}
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-shopping-bag fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Card Example --}}
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                買い物予算</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                @if (empty($budget))
                                    未設定
                                @else
                                    {{ $budget->amount }}円
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-wallet fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Card Example --}}
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                欲しいもの合計金額</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                @if (empty($wish_list))
                                    未設定
                                @else
                                    {{ $wish_list->sum('price') }}円
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    {{-- Content Row --}}
    <div class="row">
        {{-- Area Chart --}}
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                {{-- Card Header - Dropdown --}}
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">欲しいものリスト</h6>
                </div>
                {{-- Card Body --}}
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- 欲しいものリスト --}}
                    <table class="table">
                        @foreach ($wish_list as $index => $wishThing)
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ $wishThing->name }}</td>
                            <td>{{ $wishThing->price }}円</td>
                            {{--  値段が予算以下なら購入ボタンも表示  --}}
                            <td>
                            @if (isset($budget) && $wishThing->price < $budget->amount)
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#purchaseModal">
                                    <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="top" title="購入する"></i>
                                </a>
                                <form id="purchase-form" action="{{ route('purchase', $wishThing->id) }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
