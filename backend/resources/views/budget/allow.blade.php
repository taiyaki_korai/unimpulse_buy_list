@extends('layouts.app')

@section('content')

    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    </div>

    <div class="row justify-content-center">
        {{-- Area Chart --}}
        <div class="col-xl-8 col-lg-12">

            <div class="card shadow mb-4">
                <form method="POST" action="#" class="user">
                    @csrf

                    {{-- Card Header --}}
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    </div>


                    {{-- Card Body --}}
                    <div class="card-body">
                        <div class="container">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="text-center">
                                <h6>衝動を抑えてよくお金を貯めました</h6>
                                <h2 class="font-weight-bold">
                                    {{ $wishThing->name }}は<br>買ってよし！
                                </h2>
                            </div>
                            

                            <button type="button" class="btn btn-primary btn-user btn-block"
                                data-toggle="modal" data-target="#purchaseModal">
                                {{ __('購入する') }}
                            </button>
                            <a href="{{ route('dashboard') }}" class="btn btn-outline-secondary btn-user btn-block">
                                {{ __('ダッシュボードへ戻る') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="purchase-form" action="{{ route('purchase', $wishThing->id) }}" method="POST" style="display: none;">
        @csrf
    </form>
@endsection
