@extends('layouts.app')

@section('content')
    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">買い物予算更新</h1>
    </div>

    <div class="row justify-content-center">
        {{-- Area Chart --}}
        <div class="col-xl-8 col-lg-12">

            <div class="card shadow mb-4">
                <form method="POST" action="{{ route('budget.update', $budget->id) }}" class="user">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    {{-- Card Header --}}
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    </div>


                    {{-- Card Body --}}
                    <div class="card-body">
                        <div class="container">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="mt-2" for="amount">
                                    <h3>現在の買い物予算</h3>
                                </label>
                                <input type="number"
                                    class="mb-2 form-control form-control-user"
                                    id="amount_old" name="amount_old" value="{{ $budget->amount }}" readonly>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="mt-2" for="amount">
                                    <h3>新しい買い物予算</h3>
                                </label>
                                <input type="number"
                                    class="mb-2 form-control form-control-user @error('amount') is-invalid @enderror"
                                    id="amount" name="amount" value="">
                                @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <hr>
                            <button type="submit" class="mt-4 btn btn-primary btn-user btn-block">
                                {{ __('更新する') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
