@extends('layouts.app')

@section('content')

    {{-- Page Heading --}}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    </div>

    <div class="row justify-content-center">
        {{-- Area Chart --}}
        <div class="col-xl-8 col-lg-12">

            <div class="card shadow mb-4">
                @csrf

                {{-- Card Header --}}
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                </div>


                {{-- Card Body --}}
                <div class="card-body">
                    <div class="container">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            <h2 class="text-center font-weight-bold text-nowrap">
                                貯金してえらい
                            </h2>
                            @if ($shortage > 0)
                            <h6 class="text-center text-nowrap">
                                {{ $suggest_wish_thing->name }}が買えるようになるまで<br>
                                あと{{ $shortage }}円
                            </h6>
                            @endif
                        </div>
                        
                        <a href="{{ route('dashboard') }}" class="btn btn-outline-secondary btn-user btn-block">
                            {{ __('ダッシュボードへ戻る') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
