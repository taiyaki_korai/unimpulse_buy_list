<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SB Admin 2') }}</title>

    {{-- Custom fonts for this template --}}
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    {{-- Custom styles for this template --}}
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">


</head>

<body id="page-top">
    {{-- Page Wrapper --}}
    <div id="wrapper">

        @include('elements.sidebar')

        {{-- Content Wrapper --}}
        <div id="content-wrapper" class="d-flex flex-column">

            {{-- Main Content --}}
            <div id="content">

                @include('elements.topbar')

                {{-- Begin Page Content --}}
                <div class="container-fluid">
                    {{-- フラッシュメッセージ --}}
                    <div id="flashContainer">
                        
                        @if (session('flash'))
                        <div role="alert" class="mb-3 alert alert-{{ session('flash.type') }} alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! nl2br(e(session('flash.message'))) !!}
                        </div>
                        @endif
                    </div>

                    {{-- コンテンツ --}}
                    <main class="py-4">
                        @yield('content')
                    </main>

                </div>
                {{-- /.container-fluid --}}

            </div>
            {{-- End of Main Content --}}

            @include('elements.footer')

        </div>
        {{-- End of Content Wrapper --}}

    </div>
    {{-- End of Page Wrapper --}}

    @include('elements.scroll_to_top_button')
    @include('elements.modals.logout_modal')
    @include('elements.modals.purchase_modal')
    @include('elements.modals.delete_wish_thing_modal')
    @include('elements.modals.reset_purchase_modal')

    {{-- Bootstrap core JavaScript --}}
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    {{-- Core plugin JavaScript --}}
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    {{-- Custom scripts for all pages --}}
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

    {{-- Page level plugins --}}
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>

    {{-- Page level custom scripts --}}
    <script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>

    {{-- tooltip scripts --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();                                                                                                                                                                                             
        })
    </script>

</body>

</html>
