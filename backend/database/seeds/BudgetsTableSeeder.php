<?php

use App\Models\Budget;
use Illuminate\Database\Seeder;

class BudgetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Budget::create([
            'id'            => 1,
            'user_id'       => 1,
            'amount'        => 10000,
            'created_at'    => now(),
        ]);
    }
}
