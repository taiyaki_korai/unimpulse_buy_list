<?php

use App\Models\WishThing;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class WishThingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('ja_JP');

        for ($i = 0; $i < 10; $i++) {
            WishThing::create([
                'id'                => $i + 1,
                'user_id'           => 1,
                'name'              => $faker->word(),
                'price'             => round(rand(100, 10000), -2),
                'priority'          => rand(1, 100),
                'time_limit'        => null,
                'purchase_date'     => $faker->randomElement([null, null, $faker->dateTimeBetween('-2 week', new DateTime())]),
                'created_at'        => now(),
            ]);
        }
    }
}
