<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'                => 1,
            'name'              => 'test_user1',
            'email'             => 'test1@example.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('Password'),
            'created_at'        => now(),
        ]);

        User::create([
            'id'                => 2,
            'name'              => 'test_user2',
            'email'             => 'test2@example.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('Password'),
            'created_at'        => now(),
        ]);
    }
}
