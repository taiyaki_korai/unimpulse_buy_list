<?php

use App\Models\WishThing;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(BudgetsTableSeeder::class);
        $this->call(WishThingsTableSeeder::class);
    }
}
