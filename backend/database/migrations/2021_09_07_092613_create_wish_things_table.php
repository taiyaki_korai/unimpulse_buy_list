<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWishThingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //マイグレーション時の外部キー制約の無効化
        Schema::disableForeignKeyConstraints();

        Schema::create('wish_things', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->integer('price');
            $table->integer('priority');
            $table->dateTime('time_limit')->nullable();
            $table->dateTime('purchase_date')->nullable();
            $table->timestamps();

            //外部キーの設定
            $table->foreign('user_id', 'wish_things_user_id_foreign')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wish_things');
    }
}
