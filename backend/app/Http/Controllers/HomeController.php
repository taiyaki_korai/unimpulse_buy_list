<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use App\Models\WishThing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $wish_list = WishThing::query()
            ->where('user_id', Auth::id())
            ->orderBy('priority', 'desc')
            ->orderBy('price', 'asc')
            ->whereNull('purchase_date')
            ->get();

        $budget = Budget::query()
            ->where('user_id', Auth::id())
            ->first();

        return view('dashboard', compact(
            'wish_list',
            'budget'
        ));
    }
}
