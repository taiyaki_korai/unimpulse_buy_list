<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use Illuminate\Http\Request;
use App\Http\Requests\BudgetRequest;
use Illuminate\Support\Facades\Auth;
use App\Services\WishThingService;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
        $budget = Budget::query()
            ->where('user_id', Auth::id())
            ->first();
        
        return view('budget.edit', compact(
            'budget'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BudgetRequest $request, WishThingService $wishThingService, Budget $budget)
    {
        $budget->amount = $request->amount;
        
        try {

            \DB::transaction(function () use ($budget) {
                $budget->save();
            });
            //$suggest_wish_thing = $service->suggestWishThing();

            $target_root = null; // 遷移先

            if ( $wishThingService->calcShortage() >= 0 ) {
                // 予算が欲しいものの価格以上であれば購入許可画面へ遷移
                $target_root = route('budget.allow');
                
            } else {
                $target_root = route('budget.disallow');
            }

            return redirect($target_root)
                ->with('flash', ['type' => 'success', 'message' => __('messages.budget.edit.success')]);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->withInput($request->input())
                ->with('flash', ['type' => 'danger', 'message' => __('messages.budget.edit.failed')]);
        }
    }

    /**
     * 購入許可画面表示
     */
    public function allow(WishThingService $service)
    {
        $wishThing = $service->suggestWishThing();
        return view('budget.allow', compact(
            'wishThing'
        ));
    }
    /**
     * 購入不許可画面表示
     */
    public function disallow(WishThingService $service)
    {
        $suggest_wish_thing = $service->suggestWishThing();
        $shortage = -($service->calcShortage());

        return view('budget.disallow', compact(
            'suggest_wish_thing',
            'shortage'
        ));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
