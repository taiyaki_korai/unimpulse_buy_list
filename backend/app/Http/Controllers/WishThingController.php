<?php

namespace App\Http\Controllers;

use App\Models\WishThing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\WishThingRequest;
use App\Services\WishThingService;

class WishThingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * 欲しいもの追加画面表示
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wish_thing.add');
    }

    /**
     * 購入許可画面表示
     *
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function allow(WishThing $wishThing)
    {
        return view('wish_thing.allow', compact(
            'wishThing'
        ));
    }

    /**
     * 購入不許可画面表示
     *
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function disallow(WishThing $wishThing, WishThingService $service)
    {
        $this_wish_thing = $wishThing;
        $suggest_wish_thing = $service->suggestWishThing();
        $shortage = $service->calcShortage();

        return view('wish_thing.disallow', compact(
            'this_wish_thing',
            'suggest_wish_thing',
            'shortage',
        ));
    }

    /**
     * 欲しいものリスト画面表示
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $wish_list = WishThing::query()
            ->where('user_id', Auth::id())
            ->orderBy('priority', 'desc')
            ->orderBy('price', 'asc')
            ->whereNull('purchase_date')
            ->get();

        return view('wish_thing.list', compact(
            'wish_list',
        ));
    }

    /**
     * 買ったもの履歴画面表示
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
        $purchased_list = WishThing::query()
            ->where('user_id', Auth::id())
            ->orderBy('purchase_date', 'desc')
            ->whereNotNull('purchase_date')
            ->get();

        return view('wish_thing.history', compact(
            'purchased_list',
        ));
    }


    /**
     * Store a newly created resource in storage.
     * 欲しいもの追加機能
     *
     * @param  \App\Http\Requests;WishThingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WishThingRequest $request, WishThingService $wishThingService)
    {
        $wishThing = new WishThing();
        $wishThing->fill($request->all());
        $wishThing->user_id = Auth::id();
        
        try {

            \DB::transaction(function () use ($wishThing) {
                $wishThing->save();
            });

            $target_root = null; // 遷移先

            if ( empty(Auth::user()->budget) ) {
                // 買い物予算が未設定であればダッシュボードへ遷移
                $target_root = route('dashboard');
        
            } elseif ( $wishThingService->isShouldBuy($wishThing) ) {
                // 登録した欲しいものが一番買うべきものであれば購入許可画面へ遷移
                $target_root = route('wish_thing.allow', $wishThing->id);
                
            } else {
                $target_root = route('wish_thing.disallow', $wishThing->id);
            }

            return redirect($target_root)
                ->with('flash', ['type' => 'success', 'message' => __('messages.wish_thing.add.success')]);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->withInput($request->input())
                ->with('flash', ['type' => 'danger', 'message' => __('messages.wish_thing.add.failed')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function show(WishThing $wishThing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * 欲しいもの編集画面表示
     *
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function edit(WishThing $wishThing)
    {
        return view('wish_thing.edit', compact(
            'wishThing'
        ));
    }

    /**
     * Update the specified resource in storage.
     * 欲しいもの更新処理
     * 
     * @param  \App\Http\Requests\WishThingRequest  $request
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function update(WishThingRequest $request, WishThing $wishThing)
    {
        $wishThing->fill($request->all());
        
        try {

            \DB::transaction(function () use ($wishThing) {
                $wishThing->save();
            });

            return redirect(route('wish_thing.list'))
                ->with('flash', ['type' => 'success', 'message' => __('messages.wish_thing.edit.success')]);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->withInput($request->input())
                ->with('flash', ['type' => 'danger', 'message' => __('messages.wish_thing.edit.failed')]);
        }

    }

    /**
     * Remove the specified resource from storage.
     * 欲しいもの削除処理
     *
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function destroy(WishThing $wishThing)
    {        
        try {

            \DB::transaction(function () use ($wishThing) {
                $wishThing->delete();
            });

            return redirect(route('wish_thing.list'))
                ->with('flash', ['type' => 'success', 'message' => __('messages.wish_thing.delete.success')]);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->with('flash', ['type' => 'danger', 'message' => __('messages.wish_thing.delete.failed')]);
        }

    }

    /**
     * Update the specified resource in storage.
     * 買ったもの取消処理
     *
     * @param  \App\Models\WishThing  $wishThing
     * @return \Illuminate\Http\Response
     */
    public function resetPurchase(WishThing $wishThing)
    {
        
        try {

            $wishThing->purchase_date = null;

            \DB::transaction(function () use ($wishThing) {
                $wishThing->save();
            });

            return redirect(route('wish_thing.history'))
                ->with('flash', ['type' => 'success', 'message' => __('messages.wish_thing.reset_purchase.success')]);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->with('flash', ['type' => 'danger', 'message' => __('messages.wish_thing.reset_purchase.failed')]);
        }

    }


}
