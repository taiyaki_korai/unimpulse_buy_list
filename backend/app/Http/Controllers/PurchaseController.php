<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WishThing;
use Illuminate\Support\Facades\Auth;

class PurchaseController extends Controller
{
    /**
     * 購入処理を行う
     * 
     * @param \App\Http\Request $request
     * @param \App\Models\WishThing $wishThing
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, WishThing $wishThing)
    {
        $budget = Auth::user()->budget;
    
        try {
            // すでに購入済みなら例外を投げる
            if ($wishThing->purchase_date != null)  {
                throw new \Exception('The wishThing is already purchased');
            }

            \DB::transaction(function () use ($wishThing, $budget) {
                $budget->amount = $budget->amount - $wishThing->price;
                $budget->save();
                $wishThing->purchase_date = now();
                $wishThing->save();
            });

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->withInput($request->input())
                ->with('flash', ['type' => 'danger', 'message' => __('messages.purchase.failed')]);
        }

        return redirect(route('dashboard'))
                    ->with('flash', ['type' => 'success', 'message' => __('messages.purchase.success')]);
    }

}
