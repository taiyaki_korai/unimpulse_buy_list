<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     * プロフィール変更画面表示
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();

        return view('user.edit', compact(
            'user'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     * パスワード変更画面表示
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {
        $user = Auth::user();

        return view('user.change_password', compact(
            'user'
        ));
    }

    /**
     * Update the specified resource in storage.
     * ユーザー情報更新処理
     * 
     * @param  \App\Http\Requests\UserRequest $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request)
    {
                
        $user = Auth::user();
        $user->fill($request->all());
        $user->password = bcrypt($request->get('password')) ?? null;
        
        try {

            \DB::transaction(function () use ($user) {
                $user->save();
            });

            return redirect(route('dashboard'))
                ->with('flash', ['type' => 'success', 'message' => __('messages.user.edit.success')]);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()
                ->withInput($request->input())
                ->with('flash', ['type' => 'danger', 'message' => __('messages.user.edit.failed')]);
        }

    }

}
