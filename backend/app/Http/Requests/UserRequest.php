<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $this->has() == $request->has()
        if ($this->has('name')) {
            return [
                'name'                  => ['required', 'max:32','string'],
                'email'                 => ['required', 'email',  Rule::unique('users')->ignore(Auth::id())],
            ];
        }

        if ($this->has('password')) {
            return [
                'old_password'          => ['required', 'min:6', 'regex:/^[!-~]+$/', 'password'],
                'password'              => ['required', 'min:6', 'regex:/^[!-~]+$/', 'confirmed'],
                'password_confirmation' => ['required', 'min:6', 'regex:/^[!-~]+$/'],
            ];
        }

    }

    public function messages()
    {
        return [
            'old_password.regex'            => ':attributeは半角英数記で入力してください。',
            'password.regex'                => ':attributeは半角英数記で入力してください。',
            'password_confirmation.regex'   => ':attributeは半角英数記で入力してください。',
            'old_password.password'         => ':attributeが現在のパスワードと異なります。',
        ];
    }

}