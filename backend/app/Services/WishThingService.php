<?php

namespace App\Services;

use App\Models\WishThing;
use Illuminate\Support\Facades\Auth;

/**
 * 欲しいものサービスクラス
 */
class WishThingService
{
    /**
     * 一番買うべきものを返す
     * TODO:同じ欲しい度なら高い方を選択するように
     *
     * @return WishThing
     */
    public function suggestWishThing()
    {
        $wishThing = WishThing::query()
            ->where('user_id', Auth::id())
            ->orderBy('priority', 'desc')
            ->orderBy('price', 'asc')
            ->whereNull('purchase_date')
            ->first();

        return $wishThing;
    }

    /**
     * 与えられた欲しいものを買うべきかどうか判定する
     *
     * @param \App\Models\WishThing $wishThing
     * @return bool
     */
    public function isShouldBuy(WishThing $wishThing)
    {
        
        $budget = Auth::user()->budget;

        if ($wishThing->price > $budget->amount) {
            return false;
        }

        if ($wishThing->id != $this->suggestWishThing()->id) {
            return false;
        }

        return true;
    }

    /**
     * 一番買うべき物の値段と買い物予算との差を返す
     *
     * @param \App\Models\WishThing $wishThing
     * @return int
     */
    public function calcShortage()
    {
        $wishThing = $this->suggestWishThing();
        $budget = Auth::user()->budget;

        $shortage = $budget->amount - $wishThing->price;

        return $shortage;
    }

}