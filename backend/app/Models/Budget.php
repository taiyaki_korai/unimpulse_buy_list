<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Budget
 * 
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property WishThing $wish_thing
 * @property User $user
 *
 * @package App\Models
 */
class Budget extends Model
{
	protected $table = 'budgets';

	protected $casts = [
		'user_id' => 'int',
		'amount' => 'int'
	];

	protected $fillable = [
		'user_id',
		'amount'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
