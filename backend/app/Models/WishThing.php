<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WishThing
 * 
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $price
 * @property int $priority
 * @property Carbon|null $time_limit
 * @property Carbon|null $purchase_date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property User $user
 * @property Collection|Budget[] $budgets
 *
 * @package App\Models
 */
class WishThing extends Model
{
	protected $table = 'wish_things';

	protected $casts = [
		'user_id' => 'int',
		'price' => 'int',
		'priority' => 'int'
	];

	protected $dates = [
		'time_limit',
		'purchase_date'
	];

	protected $fillable = [
		'user_id',
		'name',
		'price',
		'priority',
		'time_limit',
		'purchase_date'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function budgets()
	{
		return $this->hasMany(Budget::class, 'next_with_thing_id');
	}
}
