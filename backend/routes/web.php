<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return view('home');
});
Auth::routes([
    'register' => true,
    'confirm' => true,
    'verify' => false,
]);

Route::middleware('auth')->group(function () {

    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::post('/{wishThing}/purchase', 'PurchaseController')->name('purchase');


    Route::prefix('user')->name('user.')->group(function () {

        Route::get('/edit', 'UserController@edit')->name('edit');
        Route::get('/change-password', 'UserController@changePassword')->name('change_password');
        Route::put('/update', 'UserController@update')->name('update');

    });


    Route::prefix('wish-thing')->name('wish_thing.')->group(function () {

        Route::get('/create/{wishThing}/allow', 'WishThingController@allow')->name('allow');
        Route::get('/create/{wishThing}/disallow', 'WishThingController@disallow')->name('disallow');
        Route::get('/list', 'WishThingController@list')->name('list');
        Route::get('/history', 'WishThingController@history')->name('history');
        Route::put('/{wishThing}/reset-purchase', 'WishThingController@resetPurchase')->name('reset_purchase');


        // Route::resource('/', 'WishThingController');
        // 上の記述で下記のルートが全て作成される
        Route::get('/', 'WishThingController@index')->name('index');
        Route::get('/create', 'WishThingController@create')->name('create');
        Route::post('/', 'WishThingController@store')->name('store');
        // Route::get('/{wishThing}', 'WishThingController@show')->name('show');
        Route::get('/{wishThing}/edit', 'WishThingController@edit')->name('edit');
        Route::put('/{wishThing}', 'WishThingController@update')->name('update');
        Route::delete('/{wishThing}', 'WishThingController@destroy')->name('destroy');

    });
    // 仮実装　本実装時は上記のように
    /*
    Route::get('/budget/edit', function(){
        return view('budget.edit');
    })->name('budget.edit');
    */
    Route::prefix('budget')->name('budget.')->group(function () {
        Route::get('/edit/allow', 'BudgetController@allow')->name('allow');
        Route::get('/edit/disallow', 'BudgetController@disallow')->name('disallow');

        Route::get('/', 'BudgetController@index')->name('index');
        Route::get('/edit', 'BudgetController@edit')->name('edit');
        Route::put('/{budget}', 'BudgetController@update')->name('update');
    });
    
});

