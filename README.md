# Docker-Laravel README

## 補足事項
envファイルはlaravelフォルダ内のものよりプロジェクト直下のものが優先
composer,artisanコマンドはappコンテナ内
npmコマンドはwebコンテナ内で実行

## makeコマンドについて
コマンド操作を簡略化できる
```bash
例
make restart とすると
→ docker-compose down --remove-orphans
  docker-compose up -d
  を行ってくれる
```

Windowsでの使用の場合は初期設定が必要
makeコマンドの内容はMakefileを参照

### 設定方法
[参考：Windows10でmakeしたい](https://qiita.com/taki-ikat/items/f501f44a8d44e3fd6987)

設定後
`make -v`
で動作確認

動作を確認出来たら
`make winpty`
を実行（windows用のおまじないみたいなものです）




-------以下導入パッケージのREADME--------------------------------------------------------


> # docker-laravel README 🐳
> 
> ![License](https://img.shields.io/github/license/ucan-lab/docker-laravel?color=f05340)
> ![Stars](https://img.shields.io/github/stars/ucan-lab/docker-laravel?color=f05340)
> ![Issues](https://img.shields.io/github/issues/ucan-lab/docker-laravel?color=f05340)
> ![Forks](https://img.shields.io/github/forks/ucan-lab/docker-laravel?color=f05340)
> 
> ## Introduction
> 
> Build a simple laravel development environment with docker-compose.
> 
> ## Usage
> 
> ```bash
> $ git clone git@github.com:ucan-lab/docker-laravel.git
> $ cd docker-laravel
> $ make create-project # Install the latest Laravel project
> $ make install-recommend-packages # Not required
> ```
> 
> http://localhost
> 
> Read this [Makefile](https://github.com/ucan-lab/docker-laravel/blob/master/Makefile).
> 
> ## Tips
> 
> Read this [Wiki](https://github.com/ucan-lab/docker-laravel/wiki).
> 
> ## Container structure
> 
> ```bash
> ├── app
> ├── web
> └── db
> ```
> 
> ### app container
> 
> - Base image
>   - [php](https://hub.docker.com/_/php):8.0-fpm-buster
>   - [composer](https://hub.docker.com/_/composer):2.0
> 
> ### web container
> 
> - Base image
>   - [nginx](https://hub.docker.com/_/nginx):1.18-alpine
>   - [node](https://hub.docker.com/_/node):14.2-alpine
> 
> ### db container
> 
> - Base image
>   - [mysql](https://hub.docker.com/_/mysql):8.0
> 
> #### Persistent MySQL Storage
> 
> By default, the [named volume](https://docs.docker.com/compose/compose-file/#volumes) is mounted, so MySQL data remains even if the container is destroyed.
> If you want to delete MySQL data intentionally, execute the following command.
> 
> ```bash
> $ docker-compose down -v && docker-compose up
> ```
